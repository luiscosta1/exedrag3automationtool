####################################################################################
# Filename:     config.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   16.09.2021
#
# Version:      1.0
#
# Filetype:     Config file
# Description:  Configurations for Exedra G3 Automation Tool
# STATUS:       New
# Limitation:
####################################################################################

import time
from easysettings import JSONSettings


########### Environment Global Variables ##############
imported_devices_list = []

NUM_THREADS = 5

########### G3 Serial Port Settings ###################
cli_port = 'COM16'
cli_logs_port = 'COM20'
cli_binary_port = 'COM18'
cli_baudrate = 115200
cli_databits = 8
cli_stopbits = 1
cli_parity = 'None'
cli_timeout = 1

serial_logs_max_characters = 500
#######################################################
# True:  using SeCo
# False: using local USB connection
g3_cli_remote = True
############### G3 SeCo Settings ######################
seco_host = "hyperiontest.ddns.net"
seco_port = "60913"
seco_timeout = 10

seco_user = "root"
seco_password = "nightshift"
#######################################################

ow_rconf_stage_url = 'registration-ponte-stage.owlet-iot.de'
#ow_rconf_prod_url = 'registration-ponte.owlet-iot.de'
ow_rconf_url = ow_rconf_stage_url

ow_fw_upd_server = 'u.owlet-iot.de'
ow_fw_upd_notif_server = 'u.owlet-iot.de'

###### Exedra-Specific ######
exedra_apn_url = 'global.we.vp'
exedra_apn_usr = 'orange'
exedra_apn_pwd = 'orange'

exedra_fw_upd_server = 'api/v1/G3/fw'
exedra_fw_upd_notif_server = 'api/v1/G3/fw'

# Reg Server Settings
exedra_Rurl_dev		= '10.100.162'
exedra_Rurl_tst		= '10.100.98'
exedra_Rurl_prd		= '10.100.34'

# Project Server Settings
exedra_Surl_dev		= '10.100.162'
exedra_Surl_tst		= '10.100.98'
exedra_Surl_prd		= '10.100.34'

# Exedra URLs:
exedra_url_dev = 'hyperion.dev-schreder-exedra.com'
exedra_url_tst = 'hyperion.tst-schreder-exedra.com'
exedra_url_prd = 'hyperion.schreder-exedra.com'

# Exedra Bearer Tokens: Token Must be created in Exedra -> Settings -> Personal access Tokens
exedra_token_dev = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNjA4ZmI2YTg1NjBkMDMwMDIxYjlmMDcyIiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiYzU3ZTUwYTAtMzUyNS00OGFiLTk1ZTMtZWMwZmNiMDQ1ZDhhIiwiaWF0IjoxNjM1Njk0MjY5fQ.giMnSIzBw5day6CvT22KZLJF9pHJywLCkWz8R9u00EY'
exedra_token_tst = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWY5ZmZjY2ZkZDAyNGMwMDFlYmI2MzE3IiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiNDQ0MWRjYjgtMjllOC00ZTAzLTg1NTgtZGQwNjJlZDY1OWUyIiwiaWF0IjoxNjA3MzY2MDM4fQ.7OHYbE15seAO1tezfgVoZ9l5eqG0_l_BjEbdT2ABZ9E'
exedra_token_prd = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoiNWY5ZmZkYTY4MjA1YzcwMDFmZWEyNjZlIiwidGVuYW50IjoiSHlwZXJpb24iLCJ0b2tlbklkIjoiOGIzMDNkN2MtNmEzOC00MWIzLThjMzktNDczYWNkYjM3NTAzIiwiaWF0IjoxNjI5MjA0MTExfQ.mOU5kZ8sh9GRloyvySSqP6lIhbDOae_n7LoSBBMKVu0'

##############################

# Imperium Settings
imperium_url_dev   = 'hyp-shared-dev-we-device-management-api.azurewebsites.net'
imperium_url_tst   = 'hyp-shared-tst-we-device-management-api.azurewebsites.net'
imperium_url_prd   = 'hyp-shared-prd-we-device-management-api.azurewebsites.net'

imperium_hub_dev   = 'hyp-shared-dev-we-iot-hub-01'
imperium_hub_tst   = 'hyp-shared-tst-we-iot-hub-01'
imperium_hub_prd   = 'hyp-shared-prd-we-iot-hub-01'


# Azure Settings
azure_get_credentials_dev = {
    "grant_type": "client_credentials",
    "client_id": "39039fc4-c20d-4305-8ee0-b707d8182c6d",
    "client_secret": "XlE7Q~.6x4GtDd_nRaXXqsxnkgx-PCDAOrL98",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

azure_get_credentials_tst = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://51aaa53c-893b-4777-a076-01a1dbd3a284/.default"
}

azure_get_credentials_prd = {
    "grant_type": "client_credentials",
    "client_id": "dd30c8fb-25f7-485e-a0f4-b1be306521c1",
    "client_secret": "JqM9-K_5W.bA5vfKH9D3l~_8uYAsWedGp8",
    "scope": "api://11297982-ed81-4898-a0a8-bb19346b4c4f/.default"
}

imperium_bearer_token = 'empty token, no C2D possible'
imperium_bearer_token_validity = time.time()

# G3 Public Gateway Settings
g3_gateway_host_tst = '40.74.34.91'  # TST
g3_gateway_host_dev = '51.105.190.87' # DEV
g3_gateway_host_prd = '' # Not Available

#######################################################

# Default Environment shall be TST, unless it is already set in the settings.json file

environment = 'TST'
azure_get_credentials = azure_get_credentials_tst
g3_gateway_host = g3_gateway_host_tst

imperium_hub = imperium_hub_tst
imperium_url = imperium_url_tst

fw_upd_server = exedra_fw_upd_server
fw_upd_notif_server = exedra_fw_upd_notif_server

exedra_url = exedra_url_tst
exedra_Rurl = exedra_Rurl_tst
exedra_Surl = exedra_Surl_tst
exedra_token = exedra_token_tst
#######################################################


########### Runtime Global Variables ##################
DeviceID = '0013A20041BCA3FA'
DeviceIP = '12.0.55.86'
RFID = 'E00401009F5A3E6E'

fw_current = '3.32.15.100'
fw_update = '3.32.15.101'

gps_nova_sbe_lat = '38.67824'
gps_nova_sbe_long = '-9.32672'
gps_lat = gps_nova_sbe_lat
gps_long = gps_nova_sbe_long

# Sensor configuration
source_device_id = '0000000000000000'
sensor_type = 'local'
sensor_hold_time = '90'
sup_dim_feed = 'true'

# Dimfeedback values
dimfeedback_dict = {
    "EnergyMeter": "270",
    "ACPower": "89",
    "ACCurrent": "0.300",
    "ACVoltage": "240",
    "ACPowerFactor": "0.95",
    "FeedbackDIMLevel": "20",
    "MinACPower": "85",
    "MaxACPower": "90"
}

# BurningHours values
BurningHours_dict = {
    "TotalControllerRuntime": "2500",
    "TotalRuntime": "2000"  # Lamp 1
}

# ACPower Errors List
ACPower_errors_list = ['ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing',
                       'ErrPWRHigh', 'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow']

# ACPower Errors State
ACPower_errors_dict = {
    "ErrPWRPF": True,
    "ErrVoltageHigh": True,
    "ErrZXMissing": True,
    "ErrPWRHigh": True,
    "ErrVoltageLow": True,
    "ErrSAGDetected": True,
    "ErrPWRLow": True,
    "ErrNoLoad": True
}

# ACPower_errors_dict = {
#     "ErrPWRPF": False,
#     "ErrVoltageHigh": False,
#     "ErrZXMissing": False,
#     "ErrPWRHigh": False,
#     "ErrVoltageLow": False,
#     "ErrSAGDetected": False,
#     "ErrPWRLow": False,
#     "ErrNoLoad": False
# }

BrokenLamp = False

DALIBallastStatus = "LampArcPowerOn" # "LampArcPowerOn" OR "CommsError"

DNS_serverIP = '195.177.188.21'

ow_rconf_apn_url = 'reg.o3iot.fr'
ow_rconf_apn_usr = 'ow3regradius@reg-o3iot.fr.m2m'
ow_rconf_apn_pwd = 'AgPw473h9p'

ow_upd_apn_url = 'reg.o3iot.fr'
ow_upd_apn_usr = 'ow3@reg-o3iot.fr.m2m'
ow_upd_apn_pwd = 'AgPw473h9p'

lamp_type = '1-10V'

man_dim_level = '100'
man_dim_index = '1'

switch_prf_index = '1'
switch_prf = '[{"Priority":64,"LightLvl":100},{"Priority":67,"LightLvl":0,"Valid":{"Set":["[1.1.AmbientLightStatus]"]}}]'

timezone = '1'

burn_hour_timeout = '1440'
temp_time_hyst = '10'
temp_max = '70'
start_up_seq_time = '960'
photocell_enabled = 'true'
sensor_count = '10'


# Dimming Curve values
dimcurve_dict = {
    "OversizeFactor": "100",
    "MaintenanceFactor": "100",
    "MaintenanceInterval": "100000",
    "LowWattMinDim": "1",
    "LowWattMaxDim": "15",
    "HighWattMinDim": "10",
    "HighWattMaxDim": "40",
    "PowerFactorLimit": "0.5",
    "LampTypeIP": '"1-10V"',
    "DALIindex": "1",        # DALI Only
    "MinVoltage": "1",       # 1-10V Only
    "MinPower": "1",         # 1-10V Only
    "MinLumen": "10",        # DALI + 1-10V
    "MaxVoltage": "10"       # 1-10V Only
}

#######################################################

# Create settings file instance, but don't try to open the file.
settings = JSONSettings()


# Current Time, in different formats
def now_UTC():
    """
    This function returns the current time in the G3 payload format YYMMDDhhmmss
    """
    return time.strftime("%y%m%d%H%M%S")


def now_Time():
    """
    This function returns the current time in the Rabbit MQ format e.g.: 2021-08-09T16:24:39.000Z
    """
    return time.strftime("%Y-%m-%dT%H:%M:%S.000Z")


def load_settings():
    """
    This function loads the settings from the config file settings.json
    These settings are, then, synched to the config.py runtime variables
    """
    # Settings to be loaded from the Settings file
    global settings

    # The Global variables above, with pre-defined values, which may be changed in Runtime
    global DeviceID, DeviceIP, RFID, fw_current, fw_update, gps_lat, gps_long, rabbit_mq_url, \
        dimfeedback_dict, BurningHours_dict, ACPower_errors_dict, BrokenLamp, DNS_serverIP, \
        ow_rconf_apn_url, ow_rconf_apn_usr, ow_rconf_apn_pwd, lamp_type, dimcurve_dict, fw_upd_server,\
        fw_upd_notif_server, exedra_Rurl, exedra_Surl, man_dim_level, man_dim_index, switch_prf_index, timezone,\
        burn_hour_timeout, temp_time_hyst, temp_max, start_up_seq_time, photocell_enabled, DALIBallastStatus,\
        sensor_count, imperium_bearer_token, g3_gateway_host, cli_port, seco_host, seco_port, g3_cli_remote,\
        environment


    try:
        # Let's first try and load the settings saved in "settings.json"
        settings = settings.from_file('settings.json')
        print('\nSettings successfully loaded from "settings.json"')
    except FileNotFoundError:
        # File not found... Create it and save the default settings in config.py into it
        print('\nSettings file "settings.json" not found. Creating a new one...')
        settings.filename = 'settings.json'
        create_default_settings()

    try:
        DeviceID = settings.get("DeviceID")
        DeviceIP = settings.get("DeviceIP")
        RFID = settings.get("RFID")
        fw_current = settings.get("fw_current")
        fw_update = settings.get("fw_update")
        gps_lat = settings.get("gps_lat")
        gps_long = settings.get("gps_long")
        rabbit_mq_url = settings.get("rabbit_mq_url")
        dimfeedback_dict = settings.get("dimfeedback_dict")
        BurningHours_dict = settings.get("BurningHours_dict")
        ACPower_errors_dict = settings.get("ACPower_errors_dict")
        BrokenLamp = settings.get("BrokenLamp")
        DALIBallastStatus = settings.get("DALIBallastStatus")

        DNS_serverIP = settings.get("DNS_serverIP")
        #ow_rconf_url = settings.get("ow_rconf_url")
        ow_rconf_apn_url = settings.get("ow_rconf_apn_url")
        ow_rconf_apn_usr = settings.get("ow_rconf_apn_usr")
        ow_rconf_apn_pwd = settings.get("ow_rconf_apn_pwd")

        lamp_type = settings.get("lamp_type")
        dimcurve_dict = settings.get("dimcurve_dict")

        fw_upd_server = settings.get("fw_upd_server")
        fw_upd_notif_server = settings.get("fw_upd_notif_server")

        exedra_Rurl = settings.get("exedra_Rurl")
        exedra_Surl = settings.get("exedra_Surl")

        man_dim_level = settings.get("man_dim_level")
        man_dim_index = settings.get("man_dim_index")

        switch_prf_index = settings.get("switch_prf_index")
        timezone = settings.get("timezone")
        burn_hour_timeout = settings.get("burn_hour_timeout")
        temp_time_hyst = settings.get("temp_time_hyst")
        temp_max = settings.get("temp_max")
        start_up_seq_time = settings.get("start_up_seq_time")
        photocell_enabled = settings.get("photocell_enabled")
        sensor_count = settings.get("sensor_count")
        imperium_bearer_token = settings.get("imperium_bearer_token")
        g3_gateway_host = settings.get("g3_gateway_host")
        cli_port = settings.get("cli_port")
        seco_host = settings.get("seco_host")
        seco_port = settings.get("seco_port")
        g3_cli_remote = settings.get("g3_cli_remote")
        environment = settings.get("environment")

        set_environment(environment)


    except KeyError:
        # Something is wrong with the file or some setting was not found. Create new settings file
        create_default_settings()

    return True


def set_environment(env):

    global environment, exedra_url, exedra_Rurl, exedra_Surl, exedra_token,\
        imperium_hub, imperium_url, azure_get_credentials, g3_gateway_host

    if env == "DEV":
        environment = env
        # Exedra
        exedra_url = exedra_url_dev
        exedra_Rurl = exedra_Rurl_dev
        exedra_Surl = exedra_Surl_dev
        exedra_token = exedra_token_dev
        # Imperium
        imperium_hub = imperium_hub_dev
        imperium_url = imperium_url_dev
        azure_get_credentials = azure_get_credentials_dev
        # G3 Public Gateway
        g3_gateway_host = g3_gateway_host_dev
        print('Setting Environment to', environment)
        return True
    elif env == "TST":
        environment = env
        #Exedra
        exedra_url = exedra_url_tst
        exedra_Rurl = exedra_Rurl_tst
        exedra_Surl = exedra_Surl_tst
        exedra_token = exedra_token_tst
        #Imperium
        imperium_hub = imperium_hub_tst
        imperium_url = imperium_url_tst
        azure_get_credentials = azure_get_credentials_tst
        # G3 Public Gateway
        g3_gateway_host = g3_gateway_host_tst
        print('Setting Environment to', environment)
        return True
    elif env == "PRD":
        environment = env
        # Exedra
        exedra_url = exedra_url_prd
        exedra_Rurl = exedra_Rurl_prd
        exedra_Surl = exedra_Surl_prd
        exedra_token = exedra_token_prd
        # Imperium
        imperium_hub = imperium_hub_prd
        imperium_url = imperium_url_prd
        azure_get_credentials = azure_get_credentials_prd
        # G3 Public Gateway
        g3_gateway_host = g3_gateway_host_prd
        print('Setting Environment to', environment)
        return True
    else:
        print('Unknown Environment:', env)
        return False



def create_default_settings():
    """
    This function shall be called when
        - NO settings file is found
        - Error importing Attributes from file
    It creates the default settings from the ones defined in config.py
    New file is created with the .save() method
    """

    settings.set("DeviceID", DeviceID)
    settings.set("DeviceIP", DeviceIP)
    settings.set("RFID", RFID)

    settings.set("fw_current", fw_current)
    settings.set("fw_update", fw_update)

    settings.set("gps_lat", gps_lat)
    settings.set("gps_long", gps_long)

    #settings.set("rabbit_mq_url", rabbit_mq_url)

    settings.set("dimfeedback_dict", dimfeedback_dict)
    settings.set("BurningHours_dict", BurningHours_dict)
    settings.set("ACPower_errors_dict", ACPower_errors_dict)
    settings.set("BrokenLamp", BrokenLamp)
    settings.set("DALIBallastStatus", DALIBallastStatus)

    settings.set("DNS_serverIP", DNS_serverIP)
    #settings.set("ow_rconf_url", ow_rconf_url)
    settings.set("ow_rconf_apn_url", ow_rconf_apn_url)
    settings.set("ow_rconf_apn_usr", ow_rconf_apn_usr)
    settings.set("ow_rconf_apn_pwd", ow_rconf_apn_pwd)

    settings.set("lamp_type", lamp_type)
    settings.set("dimcurve_dict", dimcurve_dict)

    settings.set("fw_upd_server", fw_upd_server)
    settings.set("fw_upd_notif_server", fw_upd_notif_server)

    settings.set("exedra_Rurl", exedra_Rurl)
    settings.set("exedra_Surl", exedra_Surl)

    settings.set("man_dim_level", man_dim_level)
    settings.set("man_dim_index", man_dim_index)

    settings.set("switch_prf_index", switch_prf_index)
    settings.set("timezone", timezone)
    settings.set("burn_hour_timeout", burn_hour_timeout)
    settings.set("temp_time_hyst", temp_time_hyst)
    settings.set("temp_max", temp_max)
    settings.set("start_up_seq_time", start_up_seq_time)
    settings.set("photocell_enabled", photocell_enabled)
    settings.set("sensor_count", sensor_count)
    settings.set("imperium_bearer_token", imperium_bearer_token)
    settings.set("g3_gateway_host", g3_gateway_host)
    settings.set("cli_port", cli_port)
    settings.set("seco_host", seco_host)
    settings.set("seco_port", seco_port)
    settings.set("g3_cli_remote", g3_cli_remote)
    settings.set("environment", environment)

    try:
        settings.save()
    except EnvironmentError:
        print("\n Not possible to save or create settings file")
        return False

    return True


def save_current_settings():
    """
    This function saves the current environment settings to the settings.json file
    Returns True if save is successful
    Returns False if an error occurred
    """

    # Set "settings" with current runtime values
    settings.set("DeviceID", DeviceID)
    settings.set("DeviceIP", DeviceIP)
    settings.set("RFID", RFID)
    settings.set("fw_current", fw_current)
    settings.set("fw_update", fw_update)
    settings.set("gps_lat", gps_lat)
    settings.set("gps_long", gps_long)
    settings.set("ACPower_errors_dict", ACPower_errors_dict)
    settings.set("BrokenLamp", BrokenLamp)
    settings.set("DALIBallastStatus", DALIBallastStatus)
    settings.set("BurningHours_dict", BurningHours_dict)
    settings.set("dimfeedback_dict", dimfeedback_dict)

    settings.set("DNS_serverIP", DNS_serverIP)
    #settings.set("ow_rconf_url", ow_rconf_url)
    settings.set("ow_rconf_apn_url", ow_rconf_apn_url)
    settings.set("ow_rconf_apn_usr", ow_rconf_apn_usr)
    settings.set("ow_rconf_apn_pwd", ow_rconf_apn_pwd)

    settings.set("lamp_type", lamp_type)
    settings.set("dimcurve_dict", dimcurve_dict)

    settings.set("fw_upd_server", fw_upd_server)
    settings.set("fw_upd_notif_server", fw_upd_notif_server)

    settings.set("exedra_Rurl", exedra_Rurl)
    settings.set("exedra_Surl", exedra_Surl)

    settings.set("man_dim_level", man_dim_level)
    settings.set("man_dim_index", man_dim_index)

    settings.set("switch_prf_index", switch_prf_index)
    settings.set("timezone", timezone)
    settings.set("burn_hour_timeout", burn_hour_timeout)
    settings.set("temp_time_hyst", temp_time_hyst)
    settings.set("temp_max", temp_max)
    settings.set("start_up_seq_time", start_up_seq_time)
    settings.set("photocell_enabled", photocell_enabled)
    settings.set("sensor_count", sensor_count)
    settings.set("imperium_bearer_token", imperium_bearer_token)
    settings.set("g3_gateway_host", g3_gateway_host)
    settings.set("cli_port", cli_port)
    settings.set("seco_host", seco_host)
    settings.set("seco_port", seco_port)
    settings.set("g3_cli_remote", g3_cli_remote)
    settings.set("environment", environment)

    # Save "settings" to file
    try:
        settings.save()
    except EnvironmentError:
        print("\n Not possible to save or create settings file")
        return False

    return True
