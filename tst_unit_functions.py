####################################################################################
# Filename:     tst_unit_functions.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   13.04.2021
#
# Version:      1.0
#
# Filetype:     Unit Test Cases file
# Description:  Unit tests to validate proper behaviour of functions internal to the code
# STATUS:       New
####################################################################################

import pytest
from main import *


def test_main_set_parameter():
    response = set_parameter(1, 1)
    assert response == False

    response = set_parameter(1, 'any_string')
    assert response == False

    response = set_parameter("any_string", 1)
    assert response == False

    response = set_parameter("any_string", "")
    assert response == False

    response = set_parameter("ErrPWRLow", "false")
    assert response

    response = set_parameter("ErrSAGDetected", "true")
    assert response

    response = set_parameter("ErrPWRLow", "any_string")
    assert response == False

    response = set_parameter("BrokenLamp", "any_string")
    assert response == False

    response = set_parameter("BrokenLamp", "true")
    assert response

    response = set_parameter("BrokenLamp", "false")
    assert response

    response = set_parameter("true", "BrokenLamp")
    assert response == False

    response = set_parameter("false", "BrokenLamp")
    assert response == False

    response = set_parameter("TotalControllerRuntime", "23.4")
    assert response

    response = set_parameter("TotalRuntime", "345")
    assert response

    response = set_parameter("TotalRuntime", "0.5")
    assert response

    response = set_parameter("TotalControllerRuntime", "23.-4")
    assert response == False

    response = set_parameter("TotalRuntime___", "34-5")
    assert response == False

    response = set_parameter("23.4", "TotalControllerRuntime")
    assert response == False

    response = set_parameter("34.5", "TotalRuntime")
    assert response == False

    response = set_parameter("EnergyMeter", "23.4")
    assert response

    response = set_parameter("ACPower", "345")
    assert response

    response = set_parameter("ACCurrent", "0.5")
    assert response

    response = set_parameter("ACVoltage", "234")
    assert response

    response = set_parameter("ACPowerFactor", "0.9")
    assert response

    response = set_parameter("FeedbackDIMLevel", "594")
    assert response

    response = set_parameter("MinACPower", "544.6")
    assert response

    response = set_parameter("MaxACPower", "134")
    assert response

    pass
