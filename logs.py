####################################################################################
# Filename:     logs.py
# Author:       Luis Costa
# Copyright:    Schreder Hyperion
#
# Created On:   02.03.2021
# Last Update:  04.03.2021
#
# Version:      0.1
#
# Filetype:     Library file
# Description:  Wrapper functions to support reading and writing G3 CLI and Logs
# Status:       Under development
# Limitation:   File must be in the same location as scripts
#               Re-think serial port opening.
####################################################################################

import logging


def create_log_file (filename):

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)  # possible logging levels are: DEBUG, INFO, WARNING, ERROR, CRITICAL

    filehandler = logging.FileHandler(filename)
    filehandler.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(levelname)s : %(name)s : %(funcName)s : %(message)s')  # with func name
    filehandler.setFormatter(formatter)

    # add file handler to logger
    logger.addHandler(filehandler)

    consolehandler = logging.StreamHandler()
    consolehandler.setLevel(logging.ERROR)

    logger.addHandler(filehandler)
    logger.addHandler(consolehandler)

    return logger
