####################################################################################
# Filename:     cli.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20.09.2021
# Last Update:  21.10.2021
#
# Version:      1.0
#
# Filetype:     Library file
# Description:  Wrapper functions to support reading and writing G3 CLI and Logs
# Status:       Under development
# Limitation:   File must be in the same location as scripts
#               Re-think serial port opening.
####################################################################################

# import serial
# import pytest
# import time
# import datetime
# from config import *
from seco import *
from cli import *
from coap import *
from g3datapoints import *
from retry import retry

SECO_HOLD_TIME = 5


@retry(EnvironmentError, 4, 2, 1)
def open_g3_cli() -> bool:
    """
    Opens communication to the G3 CLI via Seco if g3_cli_remote == True.
    Otherwise, it opens the local CLI on the COM debug port
    Returns True if Successful
    """

    if config.g3_cli_remote:
        success = seco_connect()
    else:
        success = open_serial_port('cli')

    if success:
        return True
    else:
        return False


def close_g3_cli():
    """
    Closes the communication with the G3 CLI via Seco if g3_cli_remote == True.
    Otherwise, it closes the local CLI on the COM debug port
    Returns True if Successful
    """

    if config.g3_cli_remote:
        seco_close()
    else:
        close_serial_port('cli')


def trim_g3_response(data) -> str:
    """
    This is a auxiliary function, used to
    cut the received G3 response and returns only the desired response
    eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
    """

    trimmed_response = data.strip("[CLI]>")
    trimmed_response = trimmed_response.strip('\n')  # remove both '\n'

    return trimmed_response


def read_from_g3_logs(max_characters) -> str:
    """
    Reads command from G3 logs until max_characters argument is reached
    Returns the response
    """

    data = ''
    if not serial_logs.isOpen():
        serial_logs.open()

    while True:
        #  Append read data to the buffer until it reaches max_characters
        data += serial_logs.readline().decode('UTF-8')
        if len(data) > max_characters:
            break
    return data


def set_g3_time(str_time):
    """
    Writes the received time in G3 CLI via Seco if g3_cli_remote == True. Otherwise, writes via Local CLI.
    This function shall be called with config.now_UTC() as an argument
    time shall be specified in YYMMDDhhmmss format. Otherwise, response will not be ok
    Returns False if response does not contain '[TIME]'
    """
    command = 'time ' + str_time

    if config.g3_cli_remote:
        seco_write(command)
        time.sleep(SECO_HOLD_TIME)
        response = seco_read()
    else:
        write_to_cli(command)
        response = read_from_cli()

    if response is None:
        print('Could not write time in device')
        return False
    elif '[TIME]' not in response:
        print('Could not write time in device')
        return False

    return True


def write_g3_coap(coap_message) -> bool:
    """
    Sends a COAP message to the G3 CLI via Seco if g3_cli_remote == True. Otherwise, writes via Local CLI.
    Returns False if the write fails
    """
    # Build the coap message, with the correct timestamp and DeviceID
    coap_msg = create_coap_payload(coap_message, config.DeviceID)

    command = 'coap ' + coap_msg[0] + '/' + coap_msg[1] + ', ' + coap_msg[2]
    print('\n', command)

    if config.g3_cli_remote:
        seco_write(command)
        time.sleep(SECO_HOLD_TIME)
        response = seco_read()
    else:
        write_to_cli(command)
        response = read_from_cli()

    if response is None:
        print('\n[COAP] Error Writing COAP message to G3 CLI')
        return False
    elif '[COAP] OK' not in response:
        print('\n[COAP] Error Writing COAP message to G3 CLI')
        return False

    return True


def set_g3_gps(latitude, longitude, accuracy=1):
    """
    Writes the received coordinates in G3 CLI
    If not specified, accuracy is set to 1 by default
    Returns False if response is not 'OK'
    """

    command = 'set gps ' + str(latitude) + ' ' + str(longitude) + ' ' + str(accuracy)  # set gps <lat> <long> <accuracy>

    if config.g3_cli_remote:
        seco_write(command)
        time.sleep(SECO_HOLD_TIME)
        response = seco_read()
    else:
        write_to_cli(command)
        response = read_from_cli()

    if 'OK' not in response:
        print('Could not write gps position in device')
        return False

    return True


def wait_g3_cli_ready() -> bool:
    """
    After G3 Reboot
    Wait for console until [CLI]> will appear at the new line.
    This means it is ready to start receiving commands
    Returns True when '[CLI]>' is found in the buffer.
    Returns False if for 5 minutes it didn't show up
    ********  NOT YET READY  ********
    """

    data = ''
    while not serial_cli.isOpen():  # Restart closes COM Port. We need to wait and retry until it re-opens
        serial_cli.open()
        time.sleep(1.0)
        continue

    starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)

    # Port is now open.
    while datetime.datetime.now() < starttime_plus_5:   # for a max of 5 minutes
        data += serial_cli.read_all().decode('UTF-8')   # check if '[CLI]>' shows up in the cli
        if '[CLI]>' not in data:
            time.sleep(1.0)
            continue
        else:
            print('CLI is ready to go!')
            return True
    # Something went wrong
    print('CLI was not ready in 5 minutes')
    return False


def read_datapoint(datapoint, instance=1) -> str:
    """
    Receives Datapoint to be read
    Returns response

    Info: Lamps contain max of 8 instances. Sensors contain max of 32 instances -> defined in resources_g3specific.py
    """

    obj = find_datapoint_in_g3_object(datapoint)

    if obj == '':  # datapoint not found in objects list
        print('Datapoint ' + datapoint + ' not found in objects list')
        return 'DATAPOINT NOT FOUND'

    # check validity of input parameters:
    if obj == 'Sensor':
        if instance > max_sensor_instances:
            print('Sensor cannot exceed ', max_sensor_instances, ' instances')
            return ''
    elif obj == 'Lamp':
        if instance > max_lamp_instances:
            print('Lamp cannot exceed ',  max_sensor_instances, ' instances')
            return ''

    # build command to send to device
    command = 'read dp ' + obj + '.' + str(instance) + '.' + datapoint

    if config.g3_cli_remote:
        seco_write(command)
        time.sleep(SECO_HOLD_TIME)
        response = seco_read()
    else:
        write_to_cli(command)
        response = read_from_cli()

    return response


def find_datapoint_in_g3_object(datapoint) -> str:
    """
    This is a auxiliary function, which
    Receives the datapoint to be read
    Returns the object which the datapoint belongs to
    eg. DeviceID datapoint belongs to G3_Controller object
    """

    # go through lists defined in g3datapoint_resources and return the object
    if datapoint in Controller:
        obj = 'Controller'
    elif datapoint in Lamp:
        obj = 'Lamp'
    elif datapoint in PositionTime:
        obj = 'PositionTime'
    elif datapoint in ACPower:
        obj = 'ACPower'
    elif datapoint in Comms:
        obj = 'Comms'
    elif datapoint in Management:
        obj = 'Management'
    elif datapoint in Sensor:
        obj = 'Sensor'
    elif datapoint in CLDriver:
        obj = 'CLDriver'
    else:
        obj = ''
        print('Datapoint ' + datapoint + ' Not found!')

    return obj


def write_datapoint(datapoint, value, instance=1) -> bool:
    """
    Writes Datapoint.      eg.: write dp Lamp.1.MinVoltage=1
    Returns True if Success (read OK from CLI after sending command)

    """

    obj = find_datapoint_in_g3_object(datapoint)

    # build command to send
    command = 'write dp ' + obj + '.' + str(instance) + '.' + datapoint + '=' + str(value)

    if config.g3_cli_remote:
        seco_write(command)
        response = seco_read()
    else:
        write_to_cli(command)
        response = read_from_cli()

    if 'OK' not in response:
        return False

    return True
