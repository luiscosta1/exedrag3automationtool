####################################################################################
# Filename:     test_e2e_device_registration_TST_55.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   09.03.2021
# Last update:  06.04.2021
#
# Version:      1.0
#
# Filetype:     Test Case file
# Description:  Test Case for device Registration in IoT HUB [TST-55]
# STATUS:       Stable
# Limitation:
####################################################################################
import config
from http_requests import *
from http_requests_exedra import *
from logs import *
#import time
#rom seco import *
#from config import *
from cli_wrapper import *
#from g3datapoints import *
import os
import pytest

config.load_settings()
global logger


def validate_response(received, expected):

    if received != expected:
        logger.info("Received Response:{}\nExpected Response:{}\nTEST FAILED\n".format(received, expected))
        close_g3_cli()
        raise AssertionError(
            "\nReceived Response:{}\nExpected Response:{}\nTEST FAILED\n".format(received, expected))
    else:
        return


@pytest.mark.e2e
def test_e2e_device_registration():

    global logger

    # create log file. This must be done here so the logfile gets the test __name__
    log_filename = 'logs/' + __name__ + time.strftime(" - %d%b%Y %H.%M.%S") + '.log'
    if not os.path.exists("logs"):
        os.makedirs("logs")

    logger = create_log_file(log_filename)

    # Connect to the G3 Device
    success = open_g3_cli()

    assert success, "Failed to Open communication to G3 CLI\nTEST FAILED"

    # set device time to local UTC_TIME
    success = set_g3_time(config.now_UTC())
    if not success:
        logger.info("Failed write TIME to G3 Device\nTEST FAILED")
        close_g3_cli()
        raise AssertionError("Failed write TIME to G3 Device\nTEST FAILED")
    else:
        logger.info("UTC Time written via CLI command")

    # send Rconf via cli coap
    success = write_g3_coap("exedra_reg_server")
    if not success:
        logger.info("Failed write exedra_reg_server to G3 Device via COAP\nTEST FAILED")
        close_g3_cli()
        raise AssertionError("Failed write exedra_reg_server to G3 Device via COAP\nTEST FAILED")
    else:
        logger.info("exedra_reg_server written via CLI Coap")

    time.sleep(5)

    # confirm Registration Server Rconf datapoints on the device
    response = read_datapoint('RAPN')
    expected_response = config.exedra_apn_url
    validate_response(response, expected_response)

    response = read_datapoint('RAPNUser')
    expected_response = config.exedra_apn_usr
    validate_response(response, expected_response)

    response = read_datapoint('RAPNPW')
    expected_response = config.exedra_apn_pwd
    validate_response(response, expected_response)

    response = read_datapoint('RSURL')
    expected_response = config.exedra_Rurl + '.1'
    validate_response(response, expected_response)

    response = read_datapoint('RSuser')
    expected_response = ''
    validate_response(response, expected_response)

    response = read_datapoint('RSpw')
    expected_response = ''
    validate_response(response, expected_response)

    logger.info("Registration server parameters successfully written via CLI Coap")

    # force gps cli command for now
    set_g3_gps(config.gps_lat, config.gps_long)

    time.sleep(10)

    # check gps datapoints
    response = read_datapoint('POSLAT')
    expected_response = gps_lat
    validate_response(response, expected_response)

    response = read_datapoint('POSLON')
    expected_response = gps_long
    validate_response(response, expected_response)

    logger.info("GPS Coordinates successfully written via CLI Coap")

    # send coap_reReg - > When setting the GPS positions, this will trigger a new registration. No need to force here
    success = write_g3_coap("re_register")
    if not success:
        logger.info("Failed write Re-Register to G3 Device via COAP\nTEST FAILED")
        close_g3_cli()
        raise AssertionError("Failed write Re-Register to G3 Device via COAP\nTEST FAILED")
    else:
        logger.info("Re-Register written via CLI Coap")

    logger.info("Re-Registration message successfully written via CLI Coap")

    logger.info("Waiting 5 minutes for registration")
    time.sleep(5 * 60)

    # confirm device is in IoT HUB
    device_in_iot_hub = imperium_request_device_id(DeviceID)
    if not device_in_iot_hub:
        logger.info("Device not in IoT HUB!\nTEST FAILED")
        close_g3_cli()
        raise AssertionError("Device not in IoT HUB!\nTEST FAILED")
    else:
        logger.info("Device is present in IoT HUB")

    # confirm device shows up in Exedra
    device_in_exedra = exedra_request_device_id(DeviceID)
    if not device_in_exedra:
        logger.info("Device not in Exedra!\nTEST FAILED")
        close_g3_cli()
        raise AssertionError("Device not in Exedra!\nTEST FAILED")
    else:
        logger.info("Device is present in Exedra")

    # confirm Project Server Sconf datapoints on the device
    response = read_datapoint('APN')
    expected_response = config.exedra_apn_url
    validate_response(response, expected_response)

    response = read_datapoint('APNUser')
    expected_response = config.exedra_apn_usr
    validate_response(response, expected_response)

    response = read_datapoint('APNPW')
    expected_response = config.exedra_apn_pwd
    validate_response(response, expected_response)

    response = read_datapoint('SURL')
    expected_response = config.exedra_Surl_tst + '.1'
    validate_response(response, expected_response)

    response = read_datapoint('Suser')
    expected_response = ''
    validate_response(response, expected_response)

    response = read_datapoint('Spw')
    expected_response = ''
    validate_response(response, expected_response)

    logger.info("Project Server configurations correctly set on the Device")

    # read Registration status on the G3 Device.
    expected_response = '2'
    starttime_plus_5 = datetime.datetime.now() + datetime.timedelta(minutes=5)

    while datetime.datetime.now() < starttime_plus_5:  # for a max of 5 minutes
        response = read_datapoint('RegnState')
        if response != expected_response:
            continue
        else:
            # Registration state = 2
            break

    validate_response(response, expected_response)

    logger.info("Registration state = " + str(response))
    logger.info("PASS")

    pass
