####################################################################################
# Filename:     seco.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   10.09.2021
#
# Version:      1.0
#
# Filetype:     Library file
# Description:  Wrapper functions to support reading and writing from SeCo
# Status:       Stable
# Limitation:
####################################################################################

import telnetlib
import config

# Declare SeCo Connection as a global variable, but do not open the connection
seco = telnetlib.Telnet()


def seco_connect():
    """
    This function Sets Up a Telnet connection to the SeCo
    Returns True ONLY IF the connection is successful AND no one else is using the SeCo Python Script
    Returns False if Connection is unsuccessful OR the OW3_CLI.py script failed to load in the SeCo
    """

    global seco

    # Attempt SeCo Connection. Return False if not successful
    try:
        seco = telnetlib.Telnet(config.seco_host, port=config.seco_port, timeout=config.seco_timeout)
    except Exception as exception:
        print("Communication to SeCo Unsuccessful. Exception:", exception)
        return False

    read = seco.read_until(b"login: ", timeout=config.seco_timeout)
    print(read)

    seco_write(config.seco_user)

    read = seco.read_until(b"password: ", timeout=config.seco_timeout)
    print(read)

    seco_write(config.seco_password)

    # Starting SeCo python Script to Communicate with G3
    seco_write("py OW3_CLI.py")

    read = seco.read_until(b"OW3_CLI (`!help` for help)", timeout=config.seco_timeout)
    if b"failed" in read:
        print("Failed to Start G3 Communication: SeCo is already in use!!!")

        seco_close()

        return False

    # Now we need to start communicating to the Device Under Test
    command = '!a'
    for i in range(8):
        command += config.DeviceID[2*i:2*i+2] + ':'
        # Remove trailing ':'
    command = command[:-1]

    seco_write(command)

    read = seco.read_until(b"OW3_CLI (`!help` for help)>", timeout=config.seco_timeout)

    if b"help)>" not in read:
        # return False
        raise EnvironmentError

    return True


def seco_close():
    """
    This function closes the OW3_CLI.py script in the SeCo and terminates the Telnet Connection
    Returns False if the script was not successfully closed
    Returns True otherwise
    """

    global seco

    # Exiting SeCo python Script
    seco_write("!exit")

    read = seco.read_until(b"#> ", timeout=config.seco_timeout)
    if b"#> " not in read:
        return False

    # Exiting SeCo connection
    seco_write("exit")
    # This is probably redundant
    seco.close()

    return True


def seco_write(string=''):
    """
    This function writes the received string to the G3 CLi via Seco
    """
    global seco

    # Append '\n' to confirm the command
    string += '\n'

    seco.write(string.encode('ascii'))


def seco_read():
    """
    This function reads the buffer in the SeCo
    Returns the response
    """
    global seco

    #response = seco.read_until(b"OW3_CLI (`!help` for help)", timeout=config.seco_timeout)
    #response = seco.read_lazy()
    #response = seco.read_eager()

    # This might be the reading candidate:
    response = seco.read_very_eager()

    response = trim_seco_response(response)

    return response


def trim_seco_response(data) -> str:
    """
    This is a auxiliary function, used to
    cut the received G3 response and returns only the desired response
    eg. Raw format: '\nRESPONSE\n[CLI]>' -> trimmed_response = RESPONSE
    """

    # convert from byte array to string
    data = data.decode("ascii")

    if "\r\n\r\n" in data: # This is a response from the CLI via OW3_CLI.py
        # First we discard everything until "\r\n\r\n"
        trimmed_response = data[data.find("\r\n\r\n") + 4: ] # 4 = len("\r\n\r\n")

        # Then we discard everything after "\r\n"
        trimmed_response = trimmed_response[:trimmed_response.find("\r\n")]

        return trimmed_response
    else: # This is not a response from OW3_CLI.py
        return data

