####################################################################################
# Filename:     g3_c2d_messages.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20.04.2021
#
# Version:      1.0
#
# Filetype:     Function file
# Description:  Functions required for G3 C2D message building
# STATUS:       Stable
# Limitation:
####################################################################################

def build_g3_c2d_message(coap):
    """
    This function receives the desired Coap message and encapsulates it into the body received by Imperium
    Returns the full message body to be sent to Imperium UI
    """

    # payload format example :
    # {"urlPath": "/1/1",
    #  "method": "PUT",
    #  "payload": {"Ctrl": {"UTC": "UTC_TIME", "DeviceID": "DeviceID", "ContID": 2,
    #              "Queue": [{"Priority": 64, "DALIre-init": true}]}}}

    # build command to send
    g3_message_body = '{"urlPath": "/' + coap[0] + '/' + coap[1] + '","method": "PUT","payload": ' + coap[2] + '}'
    print('\n', g3_message_body)

    return g3_message_body
