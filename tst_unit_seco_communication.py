####################################################################################
# Filename:     tst_unit_seco_communication.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   20.09.2021
#
# Version:      1.0
#
# Filetype:     Unit Test file
# Description:  Tests communication to the SeCo
# STATUS:       Stable
# Limitation:
####################################################################################
from seco import *

config.load_settings()


def test_seco_login_and_logout():

    success = seco_connect()
    assert success, "SeCo startup unsuccessful"

    # List all available G3 Nodes
    seco_write("!nodes")
    response = seco_read()
    print(response)

    success = seco_close()
    assert success, "SeCo Exit unsuccessful"


def test_seco_send_time():

    success = seco_connect()
    assert success, "SeCo startup unsuccessful"

    # List all available G3 Nodes
    seco_write("!nodes")
    response = seco_read()
    print(response)

    success = seco_close()
    assert success, "SeCo Exit unsuccessful"