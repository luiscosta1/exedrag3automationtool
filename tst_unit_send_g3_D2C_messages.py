####################################################################################
# Filename:     tst_unit_send_g3_D2C_messages.py.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   09.04.2021
#
# Version:      1.0
#
# Filetype:     Unit Test file
# Description:  Tests Sending g3 device to Exedra messages
# STATUS:       Stable
# Limitation:
####################################################################################

from g3_d2c_messages import *

config.load_settings()


def test_exedra_coap():
    host = '40.74.34.91' # TST
    #host = '51.105.190.87' # DEV
    port = 5683
    path = 'information'
    msg = '{"Info":{"UTC":"210503065138","Queue":[{"Verification":["0013A20041774B90"]}],"DeviceID":"0013A20041774B90"}}'
    msg = '{"Info":{"UTC":"210509042946","Queue":[{"Config":0,"DeltaTime":"00:01:25","DALIStatus":[],"Source":"Controller"}],"DeviceID":"0013A2004157CE6C"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"210813120255","Queue":[{"DeltaTime":"00:01:55","Priority":0,"Source":"Lamp","DimFeedback":[0.18543473,1,0.011,224.9,1,32,0,2.1],"Index":1}],"DeviceID":"0013A20041D03DC4"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"210813120255","Queue":[{"BrokenLamp":true,"DeltaTime":"00:22:11","Priority":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041D03DC4"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211011163802","Queue":[{"Config":0,"DeltaTime":"00:00:36","ErrIPCComm":false,"Source":"Controller"}],' \
          '"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'

    # DALI 0013A20041BCA3FA
    msg = '{"Info":{"UTC":"211022121326","Queue":[{"DeltaTime":"00:01:25","Priority":0,"DALI101_Error":128,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211022121326","Queue":[{"DeltaTime":"00:01:25","Priority":0,"DALI101_Error":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065045","Queue":[{"BrokenLamp":true,"DeltaTime":"00:00:53","Priority":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065200","Queue":[{"BrokenLamp":false,"DeltaTime":"00:00:53","Priority":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065045","Queue":[{"Config":0,"DeltaTime":"00:01:25","DALIStatus":["BusShort"],"Source":"Controller"}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065200","Queue":[{"Config":0,"DeltaTime":"00:01:25","DALIStatus":[],"Source":"Controller"}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065445","Queue":[{"DeltaTime":"00:01:39","Priority":0,"Source":"Lamp","DALIBallastStatus":["CommsError"],"Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065500","Queue":[{"DeltaTime":"00:01:39","Priority":0,"Source":"Lamp","DALIBallastStatus":[],"Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065545","Queue":[{"DeltaTime":"00:01:39","Priority":0,"Source":"Lamp","DALIBallastStatus":["CommsError"],"Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065645","Queue":[{"DeltaTime":"00:01:39","Priority":0,"Source":"Lamp","DALIBallastStatus":["LampArcPowerOn"],"Index":1}],"DeviceID":"0013A20041BCA3FA"},"RequestDomain":null,"Type":"information"}'

    # Multi Dali  0013A20041731091
    msg = '{"Info":{"UTC":"211025065545","Queue":[{"DeltaTime":"00:01:39","Priority":0,"Source":"Lamp","DALIBallastStatus":["CommsError"],"Index":1}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065645","Queue":[{"DeltaTime":"00:01:39","Priority":0,"Source":"Lamp","DALIBallastStatus":["LampArcPowerOn"],"Index":1}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065745","Queue":[{"DeltaTime":"00:00:01","Priority":0,"Source":"Lamp","DALIBallastStatus":["CommsError"],"Index":2}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065800","Queue":[{"DeltaTime":"00:00:01","Priority":0,"Source":"Lamp","DALIBallastStatus":[],"Index":2}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065800","Queue":[{"BrokenLamp":true,"DeltaTime":"00:00:01","Priority":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065900","Queue":[{"BrokenLamp":false,"DeltaTime":"00:00:01","Priority":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025065800","Queue":[{"BrokenLamp":true,"DeltaTime":"00:00:01","Priority":0,"Source":"Lamp","Index":2}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025065900","Queue":[{"BrokenLamp":false,"DeltaTime":"00:00:01","Priority":0,"Source":"Lamp","Index":2}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025070000","Queue":[{"DeltaTime":"00:00:01","Priority":0,"DALI101_Error":128,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025070100","Queue":[{"DeltaTime":"00:00:01","Priority":0,"DALI101_Error":0,"Source":"Lamp","Index":1}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'

    msg = '{"Info":{"UTC":"211025070000","Queue":[{"DeltaTime":"00:00:01","Priority":0,"DALI101_Error":128,"Source":"Lamp","Index":2}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'
    msg = '{"Info":{"UTC":"211025070100","Queue":[{"DeltaTime":"00:00:01","Priority":0,"DALI101_Error":0,"Source":"Lamp","Index":2}],"DeviceID":"0013A20041731091"},"RequestDomain":null,"Type":"information"}'

    response = send_d2c_exedra_coap(msg)

    assert "ACK" in response.line_print


def test_send_g3_all_errors():

    queue_list = ['BrokenLamp_1', 'BrokenLamp_2', 'ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing',
                  'ErrPWRHigh', 'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_all_ACPower_errors():

    queue_list = ['ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing', 'ErrPWRHigh',
                  'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_BrokenLamp_index_1():

    queue_list = ['BrokenLamp_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_BrokenLamp_index_2():

    queue_list = ['BrokenLamp_2']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_DALIBallastStatus_index_1():

    queue_list = ['DALIBallastStatus_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_DALIBallastStatus_index_2():

    queue_list = ['DALIBallastStatus_2']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrPWRPF():

    queue_list = ['ErrPWRPF']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrNoLoad():

    queue_list = ['ErrNoLoad']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrVoltageHigh():

    queue_list = ['ErrVoltageHigh']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrZXMissing():

    queue_list = ['ErrZXMissing']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrPWRHigh():

    queue_list = ['ErrPWRHigh']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrVoltageLow():

    queue_list = ['ErrVoltageLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrSAGDetected():

    queue_list = ['ErrSAGDetected']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_ErrPWRLow():

    queue_list = ['ErrPWRLow']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_Dimfeedback_1():

    queue_list = ['Dimfeedback_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_Dimfeedback_2():

    queue_list = ['Dimfeedback_2']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_EnergyReading():

    queue_list = ['EnergyReading']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_BurningHours():

    queue_list = ['BurningHours']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_Verification():

    queue_list = ['Verification']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_LampReg():

    queue_list = ['LampReg']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_PositionUpdate():

    queue_list = ['PositionUpdate']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_EnergyReading_and_all_errors():

    queue_list = ['ErrPWRPF', 'ErrVoltageHigh', 'ErrZXMissing', 'ErrPWRHigh',
                  'ErrVoltageLow', 'ErrSAGDetected', 'ErrPWRLow', 'EnergyReading']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_Verification_and_Dimfeedback():

    queue_list = ['Verification', 'Dimfeedback_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return


def test_send_g3_BurningHours_and_Dimfeedback():

    queue_list = ['BurningHours', 'Dimfeedback_1']

    queue = build_g3_d2c_queue(queue_list, config.DeviceID)

    # build the payload
    message_body = build_g3_d2c_message(queue, config.DeviceID)

    # send the request
    response = send_d2c_exedra_coap(message_body)

    assert "ACK" in response.line_print

    return
