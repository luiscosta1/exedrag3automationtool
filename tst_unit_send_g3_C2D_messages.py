####################################################################################
# Filename:     tst_unit_send_g3_C2D_messages.py
# Author:       Luis Costa
# E-mail:       lcosta@schreder.com
# Copyright:    Schreder Hyperion
#
# Created On:   21.04.2021
#
# Version:      1.0
#
# Filetype:     Unit Test file
# Description:  Tests Sending Imperium to G3 Device messages
# STATUS:       Stable
# Limitation:
####################################################################################
from coap import *
from csv_devices import *
from g3_c2d_messages import *
import threading
from http_requests import *

config.load_settings()


def test_send_g3_dali_reinit():

    message_body = build_g3_c2d_message(coap_dali_reinit)

    # send the request
    response = imperium_send_coap_to_device(message_body, config.DeviceID)

    assert response.status_code is 202, "Error Sending Message!"


def test_send_g3_debug_info():

    message_body = build_g3_c2d_message(coap_debug_info)

    # send the request
    response = imperium_send_coap_to_device(message_body, config.DeviceID)

    assert response.status_code is 202, "Error Sending Message!"


def test_send_g3_debug_info_to_devices_list():

    read_devices_from_csv('DeviceAssetView_Lisbon.csv') # saves to config.imported_devices_list[]

    # TODO: Multi-DALI devices will show up as duplicate in the list. Should we remove them?

    # number of devices imported
    num_devices = len(config.imported_devices_list)
    # number of threads to use
    num_threads = config.NUM_THREADS

    # this list will have the size of num_threads. each index contains the number of devices assigned to the thread
    devices_per_thread = []
    for i in range(num_threads):
        # integer division of devices per thread
        devices_per_thread.append(num_devices // num_threads)

    remainder = num_devices % num_threads
    if remainder != 0:
        # the remainder num of devices needs to be spread equally through the threads
        for i in range(remainder):
            devices_per_thread[i] += 1

    # This list will contain the device lists to be passed on to each task. It's a list of device lists
    list_of_lists = []

    # Copy all the devices on the csv list to the threads separate lists
    devices_copied = 0
    for thread in range(num_threads):
        list_of_lists.append([config.imported_devices_list[ devices_copied : (devices_copied+devices_per_thread[thread])]])
        devices_copied += devices_per_thread[thread]

    # This list will contain the threads about to be initiated
    threads_list = []

    # Test send_c2d_coap() function - REMOVE when not needed
    # send_c2d_coap(list_of_lists[thread], 'debug_info')

    # Create the threads
    for thread in range(num_threads):
        new_thread = threading.Thread(target=imperium_send_coap_to_device, args=(message_body, list_of_lists[thread]))
        threads_list.append(new_thread)

    # Start the threads
    for thread in threads_list:
        thread.start()

    # Synchronize all threads completion
    for thread in threads_list:
        thread.join()

    print("\nThreads finished")

    return
