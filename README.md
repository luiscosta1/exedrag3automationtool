# README #

This README documents the necessary steps are to get your application up and running.

More information can be found in this project's Confluence Page:
https://owlet-de.atlassian.net/wiki/spaces/CTT/pages/2715058177/Exedra+G3+Automation+Tool

### Requirements for developing this tool ###

To setup a development environment for this tool, the following requirements must be met:
* Python 3.6 or newer  
* EEL Library for the Web UI - pip install Eel
* HTTP Requests library to use the Imperium (Device Management) API - pip install requests
* Telnet library to communicate with a SeCo - pip install telnet2
* Easy Settings Library for saving and loading the settings - pip install EasySettings
* Py Installer Library to create the executable file - pip install pyinstaller
* (optional) PyTest Library for Unit Testing to the code - pip install pytest

### Building a Distributable package  ###
To create a single-file application, the following command shall be used:

* pyinstaller main.py --add-data "gui;gui" --name ExedraG3AutomationTool_[version] --onefile --icon icon.ico --clean

### Who do I talk to? ###

* Luís Costa <lcosta@schreder.com>
* Luís Gonçalves <lugoncalves@schreder.com>
* Schréder Hyperion CSS Test Team